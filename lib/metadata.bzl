visibility("//...")

ATTRS = {
    "metadata": attr.label_keyed_string_dict(
        doc = """Creates files in the repository at the path of the values with the templated contents of the keys.

Templates receive a `{{integrity}}` substitution value for exposing the archive subresource integrity.""",
        mandatory = False,
    ),
}

def metadata(rctx, canonical):
    """
    Generate metadata files based on templates passed in.

    Args:
        rctx: The download repository context.
        canonical: The final canonical arguments
    """

    if rctx.attr.metadata:
        substitutions = {"{{integrity}}": canonical["integrity"]}
        for template, output in rctx.attr.metadata.items():
            rctx.template(output, template, substitutions, executable = False)
