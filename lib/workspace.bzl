visibility("//...")

def workspace(rctx):
    """
    Retrieve the `%workspace%` replacement string.

    Args:
        rctx: The download repository context.

    Returns:
        A string to use as replacement for `%workspace%` in URIS.
    """
    root = rctx.workspace_root
    for i in range(0, 0x1eadbeef):
        if root.dirname == None:
            break
        root = root.dirname
    return str(rctx.workspace_root).replace(str(root), "/")

def write(rctx, canonical):
    """
    Generate a `WORKSPACE` file that is stamped with the canonical arguments.

    Adds the canonical arguments if a `WORKSPACE` exists.

    Does nothing if `rctx.metadata` is truthy as the user is manually deciding to write out metadata.

    Args:
        rctx: The download repository context.
        canonical: The final canonical arguments
    """
    if getattr(rctx.attr, "metadata", None):
        return

    content = "# {}".format(canonical)
    ws = rctx.path("WORKSPACE")
    if ws.exists:
        data = rctx.read("WORKSPACE", watch = "no")
        content = data + "\n" + content
    rctx.file("WORKSPACE", content, executable = False)
    return canonical
